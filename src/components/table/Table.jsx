import React from 'react';

import { Column, HeaderRow, Row } from './component';

const Table = ({ data }) => {
  const headerRowJsx = Object.entries(data[0]).map((value, i) => (
    <Column key={i} content={value[0]} />
  ));

  const rowsJsx = data.map((row, i) => {
    return (
      <Row key={i}>
        {Object.entries(row).map((value, i) => (
          <Column key={i} content={value[1]} />
        ))}
      </Row>
    );
  });

  return (
    <div className="table">
      <HeaderRow>{headerRowJsx}</HeaderRow>
      {rowsJsx}
    </div>
  );
};

export default Table;
