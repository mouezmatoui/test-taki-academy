import React from 'react';

const HeaderRow = ({ children }) => {
  return <div className="row-header">{children}</div>;
};

export default HeaderRow;
