import React from 'react';

const Column = ({ content }) => {
  return <div className="col">{content}</div>;
};

export default Column;
