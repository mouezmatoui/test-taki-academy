import React from 'react'

const Child = ({child, classes, onClick, index}) => {
  return (
    <div
      className={`child ${classes}`}
      onClick={() => onClick(index)}
      key={child.id}
    >
      <img src={child.img} alt="student" />
      <div className="about-child">
        <p className="name">{child.name}</p>
        <p className="child-id">id: {child.id}</p>
      </div>
    </div>
  )
}

export default Child