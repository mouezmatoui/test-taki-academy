import React from 'react';

import { ReactComponent as AddCircle } from '../../assets/icons/addCircle.svg';

const AddChild = () => {
  return (
    <button className="add-child" onClick={() => console.log('Add Child')}>
      <AddCircle />
    </button>
  );
};

export default AddChild;
