import React, { useState } from 'react';

import Child from './Child';
import AddChild from './AddChild';
import student1Placeholder from '../../assets/student1.svg';
import student2Placeholder from '../../assets/student2.svg';
import student3Placeholder from '../../assets/student3.svg';

const CHILDREN = [
  {
    name: 'Enfant 1',
    id: '12365',
    img: student1Placeholder,
  },
  {
    name: 'Enfant 2',
    id: '21348',
    img: student2Placeholder,
  },
  {
    name: 'Enfant 3',
    id: '98242',
    img: student3Placeholder,
  },
];

const Children = () => {
  const [index, setIndex] = useState(0);
  const childrenJsx = CHILDREN.map((child, i) => (
    <Child
      key={child.id}
      child={child}
      classes={`${index === i ? 'selected' : ''}`}
      index={i}
      onClick={setIndex}
    />
  ));

  return (
    <article className="my-children">
      <h2>Mes Enfants</h2>
      <div className="children">
        <AddChild />
        {childrenJsx}
      </div>
    </article>
  );
};

export default Children;
