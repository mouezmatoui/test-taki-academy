import React from 'react';
import ReactDOM from 'react-dom/client';
import RoutesProvider from 'routes';
import { Provider } from 'react-redux';
import { store } from './store/';
import './main.scss';

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <React.StrictMode>
    <Provider store={store}>
      <RoutesProvider />
    </Provider>
  </React.StrictMode>
);
