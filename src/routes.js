import { Home, HomeWork } from 'features';
import { Layout } from './layout';
import { BrowserRouter, Routes, Route, Navigate } from 'react-router-dom';

const RoutesProvider = () => {
  return (
    <BrowserRouter>
      <Routes>
        <Route path="/" element={<Layout />}>
          <Route index element={<Home />} />
          <Route path="en-direct" element={<h2> En Direct</h2>} />
          <Route path="travail-a-faire" element={<HomeWork />} />
          <Route path="examens" element={<h2>Examens</h2>} />
          <Route path="porte-monnaire" element={<h2>Porte monnaire</h2>} />
        </Route>
        <Route path="*" element={<Navigate to={'/'} />} />
      </Routes>
    </BrowserRouter>
  );
};

export default RoutesProvider;
