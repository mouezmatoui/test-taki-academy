const getHoursInGMT = (date) =>
  new Date(date).toLocaleTimeString('en-US', {
    hour: 'numeric',
    minute: 'numeric',
    hour12: false,
    timeZone: 'Etc/GMT',
  });

export default getHoursInGMT;
