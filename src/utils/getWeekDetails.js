import { startOfWeek, endOfWeek, eachDayOfInterval } from 'date-fns';
import getDayDetails from './getDayDetails';

const getWeekDetails = (date) => {
  const weekStarts = startOfWeek(date, { weekStartsOn: 1 });
  const weekEnds = endOfWeek(date, { weekStartsOn: 1 });
  const daysOfWeek = eachDayOfInterval({ start: weekStarts, end: weekEnds });
  const days = daysOfWeek.map((day) => {
    return getDayDetails(day);
  });

  const currentWeek = {
    start: days[0].monthDay,
    end: days[days.length - 1].monthDay,
    month: days[0].monthName,
    year: days[0].year
  };

  return {currentWeek, daysOfWeek: days};
};

export default getWeekDetails;
