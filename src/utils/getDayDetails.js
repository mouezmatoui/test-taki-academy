import { format, getDay } from 'date-fns';

const getDayDetails = (date) => {
  const day = getDay(date, 1);
  const dayName = format(date, 'EEEE');
  const monthName = format(date, 'MMMM');
  const monthDay = date.getDate();
  const monthIndex = new Date(date).getMonth();
  const year = date.getFullYear();

  return {
    day,
    dayName,
    monthName,
    monthDay,
    year,
    monthIndex,
    date,
  };
};

export default getDayDetails;
