import React from 'react';
import NavLinks from './components/NavLinks';

const Aside = ({ collapseAside }) => {
  return (
    <aside className={`${!collapseAside ? '' : 'collapse'}`}>
      <NavLinks />
    </aside>
  );
};

export default Aside;
