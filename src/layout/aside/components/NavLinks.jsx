import React from 'react';
import { NavLink } from 'react-router-dom';
import homeIcon from '../../../assets/icons/Home.svg';
import enDirectIcon from '../../../assets/icons/Videoen-direct.svg';
import aFaireIcon from '../../../assets/icons/Bullets.svg';
import examsIcon from '../../../assets/icons/File.svg';
import porteMIcon from '../../../assets/icons/Card.svg';

const NavLinks = () => {
  return (
    <ul>
      <li>
        <NavLink to="/">
          <img src={homeIcon} alt="icon" />
          <p>Accueil</p>
        </NavLink>
      </li>
      <li>
        <NavLink to="/en-direct">
          <img src={enDirectIcon} alt="icon" />
          <p>En direct</p>
        </NavLink>
      </li>
      <li>
        <NavLink to="/travail-a-faire">
          <img src={aFaireIcon} alt="icon" />
          <p>Travail a faire</p>
        </NavLink>
      </li>
      <li>
        <NavLink to="/examens">
          <img src={examsIcon} alt="icon" />
          <p>Examens</p>
        </NavLink>
      </li>
      <li>
        <NavLink to="/porte-monnaire">
          <img src={porteMIcon} alt="icon" />
          <p>Porte monnaie</p>
        </NavLink>
      </li>
    </ul>
  );
};

export default NavLinks;
