import React, { useState } from 'react';
import { Outlet } from 'react-router-dom';

import Aside from 'layout/aside';
import Header from 'layout/header';

const Layout = () => {
  const [collapseAside, setCollapseAside] = useState(false);

  const handleCollapseAside = () => setCollapseAside((prev) => !prev);
  return (
    <div className="layout">
      <Header setCollapseAside={handleCollapseAside} />
      <Aside collapseAside={collapseAside} />
      <main className={`${!collapseAside ? '' : 'collapse'}`}>
        <Outlet />
      </main>
    </div>
  );
};

export default Layout;
