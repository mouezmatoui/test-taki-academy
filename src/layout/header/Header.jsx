import React from 'react';
import {
  BurgerIcon,
  Logo,
  Search,
  Balance,
  Notifications,
  AboutMe,
} from './components';

const Header = ({ setCollapseAside }) => {
  return (
    <header>
      <div>
        <BurgerIcon onClick={setCollapseAside} />
        <Logo />
      </div>
      <Search />
      <div className="about-me">
        <Balance />
        <Notifications />
        <AboutMe />
      </div>
    </header>
  );
};

export default Header;
