import React from 'react'
import {AiOutlineSearch} from 'react-icons/ai'

const Search = () => {
  return (
    <div className='search'>
        <label htmlFor="search">
            <AiOutlineSearch  color='#2BA7DF'/>
        </label>
        <input type="search" name="search" id="search" placeholder='Search courses, exams and exercises...'/>
    </div>
  )
}

export default Search