import React from 'react'
import {AiOutlineDown} from 'react-icons/ai'

import imgPlaceholder from '../../../assets/placeholder.png'

const AboutMe = () => {
  return (
    <div className='about-user'>
      <div className="profile-pic">
        <img src={imgPlaceholder} alt="user" />
      </div>
      <div className="about-user">
        <p className="user-name">Mouez Matoui</p>
        <p className="user-class">Bac Mathematiques</p>
      </div>
      <div className="down-arrow">
        <AiOutlineDown color={'#18698D'} />
      </div>
    </div>
  )
}

export default AboutMe