import React from 'react';

import { ReactComponent as EmailSvg } from '../../../assets/icons/email.svg';

const Notifications = () => {
  return (
    <div className="notification">
      <p className="pop">0</p>
      <EmailSvg />
    </div>
  );
};

export default Notifications;
