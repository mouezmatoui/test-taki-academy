import React from 'react';

import {ReactComponent as Wallet} from '../../../assets/icons/wallet.svg';

const Balance = () => {
  return (
    <div className='balance'>
      <div className="wallet">
        <Wallet />

      </div>
      <div className="your-balance">
        <p className="solde">Your Balance</p>
        <h5 className="points">0 Pts</h5>
      </div>
    </div>
  );
};

export default Balance;
