import React from 'react';
import { ReactComponent as BurgerSvg } from '../../../assets/icons/Hamburger-Menu.svg';

const BurgerIcon = ({ onClick }) => {
  return (
    <div className="burger-icon" onClick={onClick}>
      <BurgerSvg />
    </div>
  );
};

export default BurgerIcon;
