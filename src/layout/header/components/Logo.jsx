import React from 'react';
import logo from '../../../assets/icons/taki-logo.svg';
const Logo = () => {
  return (
    <div className="logo-container">
      <img src={logo} alt="Taki Academy" />
      <span className='logo-name'>TakiAcademy</span>
    </div>
  );
};

export default Logo;
