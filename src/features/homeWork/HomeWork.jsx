import React from 'react';

import { Children } from '../../components';
import { Calendar } from './components';

const HomeWork = () => {
  return (
    <section className="home-work">
      <Children />
      <Calendar />
    </section>
  );
};

export default HomeWork;
