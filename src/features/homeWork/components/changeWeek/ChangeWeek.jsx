import React, { useEffect, useReducer } from 'react';
import getWeekDetails from 'utils/getWeekDetails';
import {
  BsChevronRight,
  BsChevronLeft,
  BsFillCalendarEventFill,
} from 'react-icons/bs';
import { MdOutlineCheck } from 'react-icons/md';

const reducer = (state, action) => {
  const sevenDaysInMl = 604800000;
  switch (action.type) {
    case 'prev':
      return { currentDate: state.currentDate - sevenDaysInMl };
    case 'next':
      return { currentDate: state.currentDate + sevenDaysInMl };
    case 'current':
      return { currentDate: Date.now() };
    default:
      return { currentDate: Date.now() };
  }
};

const ChangeWeek = ({ date, setCurrentDate }) => {
  const [{ currentDate }, dispatch] = useReducer(reducer, {
    currentDate: date,
  });
  const { currentWeek } = getWeekDetails(currentDate);
  const { start, end, month, year } = currentWeek;

  const weekHandler = (type) => {
    dispatch({ type: type });
  };

  useEffect(() => {
    setCurrentDate(currentDate);
  }, [currentDate, setCurrentDate]);

  return (
    <div className="change-week">
      <BsChevronLeft
        color="#EC5542"
        onClick={() => weekHandler('prev')}
        strokeWidth={1}
        size={22}
      />
      <p>
        {start} - {end} {month} {year}
      </p>
      <BsChevronRight
        color="#EC5542"
        strokeWidth={1}
        onClick={() => weekHandler('next')}
        size={22}
      />
      <div className="this-week" onClick={() => dispatch('current')}>
        <BsFillCalendarEventFill color="#2BA7DF" />
        <p>Cette semaine</p>
        <MdOutlineCheck color="#2BA7DF" size={22} />
      </div>
    </div>
  );
};

export default ChangeWeek;
