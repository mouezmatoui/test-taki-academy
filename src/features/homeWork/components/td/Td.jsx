import React from 'react';

const Td = ({ hour, className, children }) => {
  return <td className={`${className ? className : ''}`}>{children}</td>;
};

export default Td;
