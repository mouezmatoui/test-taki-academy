import React from 'react';

import { Th } from '..';

const Thead = ({ daysOfWeek }) => {
  return (
    <thead>
      <tr className="week-days">
        <div></div>
        {daysOfWeek.map((day) => {
          const today =
            day.monthDay === new Date().getDate() &&
            day.monthIndex === new Date().getMonth() &&
            day.year === new Date().getFullYear();
          return (
            <Th
              className={`${today ? 'current-day ' : ''}`}
              key={day.dayName}
              day={day}
            />
          );
        })}
      </tr>
    </thead>
  );
};

export default Thead;
