import React from 'react';

const Th = ({ className, day }) => {
  return (
    <th className={`day ${className}`}>
      <h5 className="day-name">{day.dayName}</h5>
      <p className="date">
        <span className="month-date">{day.monthDay}</span>
        <span className="month-name">{day.monthName}</span>
      </p>
    </th>
  );
};

export default Th;
