import React from 'react';
import Td from '../td';

const EmptyRow = ({ daysOfWeek }) => {
  return (
    <tr className="row">
      <div></div>
      {daysOfWeek.map((day) => {
        const today =
          day.monthDay === new Date().getDate() &&
          day.monthIndex === new Date().getMonth() &&
          day.year === new Date().getFullYear();
        return (
          <Td
            key={day.monthDay}
            className={`${today ? 'current-day ' : ''}`}
          ></Td>
        );
      })}
    </tr>
  );
};

export default EmptyRow;
