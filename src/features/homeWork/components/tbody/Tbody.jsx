import React, { useEffect } from 'react';
import { getEvents, allEvents } from 'store/slices/eventsSlice';
import { useSelector, useDispatch } from 'react-redux';
import getHoursInGMT from 'utils/getHoursInGMT';
import EmptyRow from './EmptyRow';
import { Event, Td } from '..';
import { hours } from '../../data';

const Tbody = ({ daysOfWeek }) => {
  const events = useSelector(allEvents);
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(getEvents());
  }, [dispatch]);

  return (
    <tbody>
      <EmptyRow daysOfWeek={daysOfWeek} />
      {hours.map((hour, hourIndex) => (
        <tr key={hour}>
          <div>{getHoursInGMT(hour)}</div>
          {daysOfWeek.map((day) => {
            const isToday =
              day.monthDay === new Date().getDate() &&
              day.monthIndex === new Date().getMonth() &&
              day.year === new Date().getFullYear();
            return (
              <Td
                className={`${isToday ? 'current-day ' : ''} ${
                  isToday && hours.length - 1 === hourIndex ? 'last-td' : ''
                }`}
                key={day.dayName}
              >
                {events.map((e, i) =>
                  JSON.stringify(new Date(e.start).getHours()) ===
                    JSON.stringify(new Date(hour).getHours()) &&
                  day.monthDay === new Date(e.start).getDate() &&
                  day.monthIndex === new Date(e.start).getMonth() ? (
                    <Event
                      height={e.duration}
                      key={i}
                      minutes={new Date(e.start).getMinutes()}
                      eventStatus={new Date(e.start)}
                      event={e}
                    />
                  ) : null
                )}
              </Td>
            );
          })}
        </tr>
      ))}
    </tbody>
  );
};

export default Tbody;
