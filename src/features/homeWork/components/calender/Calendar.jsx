import React, { useEffect, useState } from 'react';
import getWeekDetails from 'utils/getWeekDetails';
import { Nav } from 'features/home/components';
import { ChangeWeek, Tbody, Thead } from '../';

const Calendar = () => {
  const [currentDate, setCurrentDate] = useState(Date.now());
  const [{ daysOfWeek }, setDaysOfWeek] = useState(getWeekDetails(currentDate));

  useEffect(() => {
    setDaysOfWeek(getWeekDetails(currentDate));
  }, [currentDate]);

  return (
    <div className="calendar">
      <Nav text={'Travail à faire'} color={true} link={'Tous'} />
      <ChangeWeek date={currentDate} setCurrentDate={setCurrentDate} />
      <table>
        <Thead daysOfWeek={daysOfWeek} />
        <Tbody daysOfWeek={daysOfWeek} />
      </table>
    </div>
  );
};

export default Calendar;
