import React from 'react';

import getHoursInGMT from 'utils/getHoursInGMT';

const Event = ({ event }) => {
  const isFinishedEvent = new Date(event.start) < new Date();
  const height = (new Date(event.end) - new Date(event.start)) / (1000 * 60);

  const minutes = new Date(event.start).getMinutes();

  const eventClasses = `event ${height ? `event-height${height}` : ''}  ${
    minutes ? `minutes${minutes}` : ''
  } ${isFinishedEvent ? 'finished' : 'soon'}`;

  return (
    <div
      className={eventClasses}
      onClick={() => console.log('show event modal')}
    >
      <div className="event-subject">
        <img src={event.subject.image} alt="" />

        <p>
          {event.subject.name} ({event.level})
        </p>
      </div>
      <div className="event-stats">
        <div className="time">
          <p>{getHoursInGMT(event.start)} </p> <p> - </p>
          <p>{getHoursInGMT(event.end)}</p>
        </div>
        <button className={`${!isFinishedEvent && 'soon'}`}>
          {isFinishedEvent ? 'Terminé' : 'Bientôt'}
        </button>
      </div>
    </div>
  );
};

export default Event;
