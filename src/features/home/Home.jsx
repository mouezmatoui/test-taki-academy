import React, { useEffect } from 'react';
import { getChildren } from '../../store/slices/childrenSlice';
import { Live, LiveStats, Progress, Exams } from './components';
import { Children } from '../../components';
import { useDispatch } from 'react-redux';

const Home = () => {
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(getChildren());
  }, []);

  return (
    <section className="home">
      <Children />
      <div className="column">
        <Live />
        <LiveStats />
      </div>
      <div className="column">
        <Progress />
        <Exams />
      </div>
    </section>
  );
};

export default Home;
