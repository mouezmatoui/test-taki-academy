import React from 'react';
import { Link } from 'react-router-dom';
import { Nav } from '../';
import Subject from './Subject';

import { ReactComponent as DownArrow } from '../../../../assets/icons/Forward.svg';

const Progress = () => {
  return (
    <article className="progress">
      <Nav color={true} text={'Progression'} />
      <Subject />
      <Subject />
      <Subject />
      <Link to="/" className="see-more">
        <p>Voir Plus</p>
        <DownArrow />
      </Link>
    </article>
  );
};

export default Progress;
