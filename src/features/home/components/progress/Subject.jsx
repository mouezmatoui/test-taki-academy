import React from 'react';

import ProgressBar from './ProgressBar';
import AboutSubject from './AboutSubject';

const Subject = () => {
  return (
    <div className="subject">
      <AboutSubject />
      <ProgressBar />
    </div>
  );
};

export default Subject;
