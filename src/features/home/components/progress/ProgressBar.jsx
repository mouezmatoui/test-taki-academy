import React from 'react'

const ProgressBar = () => {
  return (
    <div className="progress-bar">
      <span className="current-progress-50"></span>
    </div>
  )
}

export default ProgressBar