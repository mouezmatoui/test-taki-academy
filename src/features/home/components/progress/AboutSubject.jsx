import React from 'react';

import {ReactComponent as SubjectPlaceholder} from '../../../../assets/subjectPlaceholder.svg';

const AboutSubject = () => {
  return (
    <div className="about-subject">
      <div className="sub-name">
        <SubjectPlaceholder />

        <p>Matière 1</p>
      </div>
      <p className="percentage">50%</p>
    </div>
  );
};

export default AboutSubject;
