import React from 'react';
import Nav from '../nav';
import { Table } from '../../../../components';

const TABLE_DATA = [
  {
    date: '28/01/2022',
    Nom: 'Séance 1',
    Matière: 'Matière',
    note: 15.5,
  },
  {
    date: '28/01/2022',
    Nom: 'Séance 2',
    Matière: 'Matière',
    note: '--',
  },
  {
    date: '28/01/2022',
    Nom: 'Séance 3',
    Matière: 'Matière',
    note: 16,
  },
];

const Exams = () => {
  return (
    <article className="exams">
      <Nav text={'Examens'} color={true} underLine={true} link="examens" />
      <Table data={TABLE_DATA} />
    </article>
  );
};

export default Exams;
