import React from 'react';
import { Link } from 'react-router-dom';
import {ReactComponent as Cast} from '../../../../assets/icons/Cast.svg';
import {ReactComponent as ArrowRight} from '../../../../assets/icons/arrowRight.svg';

const Nav = ({ text, link, color, underLine }) => {
  return (
    <nav className="live-nav">
      <h2 className={`${underLine && 'under-line'} ${color && 'blue'}`}>
        <Cast />
        <span>{text}</span>
      </h2>
      {!link ? null : (
        <Link to={link} className="see-more">
          <p>Voir plus</p>
          <span>
            <ArrowRight />
          </span>
        </Link>
      )}
    </nav>
  );
};

export default Nav;
