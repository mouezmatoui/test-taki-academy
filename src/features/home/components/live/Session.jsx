import React from 'react'
import {ReactComponent as TimeIcon} from '../../../../assets/icons/Time.svg';


const Session = ({time = "Aujourd'hui - 13:00", subject = "Matière", teacher = "Professeur"}) => {
  return (
    <div className="session">
    <h5 className="time">
      <TimeIcon />
      {time}
    </h5>
    <p>Matière: {subject}</p>
    <p>Présenté par {teacher}</p>
  </div>
  )
}

export default Session