import React from 'react';
import Session from './Session';
import Nav from '../nav';

const Live = () => {
  return (
    <article className="live">
      <Nav text={"Séances en direct d'aujourd'hui"} link="travail-a-faire" />
      <Session />
      <Session />
    </article>
  );
};

export default Live;
