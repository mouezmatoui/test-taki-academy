import React from 'react';

import { Nav } from '..';
import { Table } from '../../../../components';

const TABLE_DATA = [
  {
    date: '28/01/2022 16:00',
    session: 'Séance 1',
    duration: '01:55:05 ',
  },
  {
    date: '28/01/2022 16:00',
    session: 'Séance 2',
    duration: '01:55:05 ',
  },
  {
    date: '28/01/2022 16:00 ',
    session: 'Séance 3',
    duration: '01:55:05 ',
  },
];

const LiveStats = () => {
  return (
    <article className="live-statics">
      <Nav
        text="Séances en direct statistiques"
        link="en-direct"
        color={true}
        underLine={true}
      />
      <Table data={TABLE_DATA} />
    </article>
  );
};

export default LiveStats;
