import { configureStore } from '@reduxjs/toolkit';
import childrenReducer from './slices/childrenSlice';
import eventsReducer from './slices/eventsSlice';

export const store = configureStore({
  reducer: {
    children: childrenReducer,
    events: eventsReducer,
  },
});
