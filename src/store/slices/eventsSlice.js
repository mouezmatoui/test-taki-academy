import { createAsyncThunk, createSlice } from '@reduxjs/toolkit';
import axiosInstance from '../../utils/axios';

export const getEvents = createAsyncThunk('/', async () => {
  let data;
  try {
    const response = await axiosInstance.get('/events');
    data = await response.data;
    if (response.status === 200 || 'OK') {
      return data;
    }
    throw new Error(response.statusText);
  } catch (err) {
    const error = err;
    return Promise.reject(error.message ? error.message : data?.message);
  }
});

const initialState = {
  events: [],
  status: 'idle', // 'loading' | 'succeeded' | 'failed'
  error: null,
};

const eventsSlice = createSlice({
  name: 'events',
  initialState,
  extraReducers: (builder) => {
    builder
      .addCase(getEvents.pending, (state, action) => {
        state.status = 'pending';
      })
      .addCase(getEvents.fulfilled, (state, action) => {
        state.events = action.payload;
        state.status = 'success';
      })
      .addCase(getEvents.rejected, (state, action) => {
        state.status = 'failed';
        state.error = action.error.message;
      });
  },
});

export const allEvents = (state) => state.events.events;

export default eventsSlice.reducer;
