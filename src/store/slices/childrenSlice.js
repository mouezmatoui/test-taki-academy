import { createAsyncThunk, createSlice } from '@reduxjs/toolkit';
import axiosInstance from '../../utils/axios';

export const getChildren = createAsyncThunk('/', async () => {
  let data;
  try {
    const response = await axiosInstance.get('/children');
    data = await response.data;
    if (response.status === 200 || 'OK') {
      return data;
    }
    throw new Error(response.statusText);
  } catch (err) {
    const error = err;
    return Promise.reject(error.message ? error.message : data?.message);
  }
});

const initialState = {
  children: [],
  status: 'idle', // 'loading' | 'succeeded' | 'failed'
  error: null,
};

const childrenSlice = createSlice({
  name: 'children',
  initialState,
  extraReducers: (builder) => {
    builder
      .addCase(getChildren.pending, (state, action) => {
        state.status = 'pending';
      })
      .addCase(getChildren.fulfilled, (state, action) => {
        state.events = action.payload;
        state.status = 'success';
      })
      .addCase(getChildren.rejected, (state, action) => {
        state.status = 'failed';
        state.error = action.error.message;
      });
  },
});

export default childrenSlice.reducer;
